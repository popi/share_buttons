/* This is a derivative work based on Aly-ve work
 * https://aly-ve.github.io/Mastodon-share-button/
 *
 * Original source code for Mastodon-share-button can be
 * found at https://github.com/Aly-ve/Mastodon-share-button
 *
 * This code is free software and released under AGPLv3 licence.
 *
*/
var msbConfig = {
  addressFieldSelector: '#msb-address',
  buttonModalSelector: '#msb-share',
  memorizeFieldId: 'msb-memorize-instance',
};

"use strict";

const COOKIE_ADDRESS = 'instance-address'
const COOKIE_PLATFORM = 'instance-platform'
const URL_REGEX = /^(https?:\/\/)?([\da-z.-]+)\.([a-z.]{2,6})([\/\w .-]*)*\/?$/

function msbShareButtonAction(name, target, platform) {
  let msbInstanceAddress = ''

  if (msbConfig && msbConfig.addressFieldSelector) {
    msbOnShare(name, target)
  }
}

function msbOnShare(_name, _target) {
  if (msbConfig && msbConfig.addressFieldSelector && msbConfig.buttonModalSelector) {

    let name = !!_name ? _name : document.querySelector(msbConfig.buttonModalSelector).data.name
    let target = !!_target ? _target : document.querySelector(msbConfig.buttonModalSelector).data.target
    let msbInstanceAddress = document.querySelector(`${msbConfig.addressFieldSelector}`).value
    let msbPlatform = document.querySelector('input[name = "platform"]:checked').value

    if (msbInstanceAddress.match(URL_REGEX)) {
      if (msbConfig.memorizeFieldId) {
        let msbMemorizeIsChecked = document.querySelector(`#${msbConfig.memorizeFieldId}`).checked
        // adding an extra level to allow overwritting the cookie
        if (msbMemorizeIsChecked) {
          // Handling cookie for URL
          if (msbGetCookie(COOKIE_ADDRESS).length > 0) {
            // if new address is different than cookie
            if (msbInstanceAddress != msbGetCookie(COOKIE_ADDRESS) ) {
              msbSetCookie(COOKIE_ADDRESS, msbInstanceAddress, 7);
            }
          } else {
            msbSetCookie(COOKIE_ADDRESS, msbInstanceAddress, 7);
          }
          // Handling cookie for Platform
          if (msbGetCookie(COOKIE_PLATFORM).length > 0) {
            // if new platform is different than cookie
            if (msbPlatform != msbGetCookie(COOKIE_PLATFORM) ) {
              msbSetCookie(COOKIE_PLATFORM, msbPlatform, 7);
            }
          } else {
            msbSetCookie(COOKIE_PLATFORM, msbPlatform, 7);
          }
        }
      }

      // Several platform use the same sharing URLs, but we keep them all separated for readability
      // and consistency purposes.
      switch(msbPlatform) {
        case "pleroma":
          window.open(`${msbInstanceAddress}/share?message=${name}%20${target}`, `__blank`)
          break;
        case "mastodon":
          window.open(`${msbInstanceAddress}/share?text=${name}%20${target}`, `__blank`)
          break;
        case "gnusocial":
          window.open(`${msbInstanceAddress}/?action=newnotice&status_textarea=${name}%20${target}`, `__blank`)
          break;
        case "friendica":
          window.open(`${msbInstanceAddress}/bookmarklet?url=${target}&title=${name}&jump-doclose`, `__blank`)
          break;
        case "diaspora":
          window.open(`${msbInstanceAddress}/bookmarklet?url=${target}&title=${name}&jump-doclose`, `__blank`)
          break;
        case "misskey":
          window.open(`${msbInstanceAddress}/share?text=${name}%20${target}`, `__blank`)
          break;
        default:
          alert("nothing chosen! should not happen...");
      }
    } else {
      document.getElementById('no_url').className = "footnote warning";
      document.getElementById('no_url').style = "margin-bottom: 0";
      document.getElementById('no_url').innerHTML = "Please enter the domain of your instance.";
    }
  }
}

function msbGetCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
          c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
      }
  }
  return "";
}

function msbSetCookie(name, value, days) {
  let d = new Date()
  d.setTime(d.getTime() + days*86400000)
  let expires = 'expires=' + d.toUTCString()
  document.cookie = `${name}=${value}; ${expires}; path=/`
}

(function() {

  // get the first occurrence
  let msbButtons = document.querySelector('#msb-share')
  let msbTarget = msbButtons.dataset.target
  let msbName = msbButtons.dataset.name
  let msbInstanceAddress = msbGetCookie('instance-address')
  let msbPlatform = msbGetCookie('instance-platform')

  // Fill the form for URL and radio button from Cookie (if applicable)
  document.getElementById("msb-address").value = msbInstanceAddress;

  var platformRadio = document.getElementsByName('platform');
  for(var i = 0; i < platformRadio.length; i++){
    if(platformRadio[i].value == msbPlatform){
        platformRadio[i].checked = true;
        break;
    }
  }
  // Replace hashtab by html code
  msbName = msbName.replace(/#/g, '%23')

  msbButtons.addEventListener('click', () => { msbShareButtonAction(msbName, msbTarget, msbPlatform) }, true)
})()

function msbI18n() {
  let language = navigator.language || navigator.userLanguage
  let publish = {
    'ar': 'بوّق',
    'bg': 'Раздумай',
    'cs': 'Tootnout',
    'de': 'Tröt',
    'eo': 'Hué',
    'es': 'Tootear',
    'eu': 'Tut',
    'fa': 'بوق',
    'fi': 'Tuuttaa',
    'fr': 'Pouet',
    'gl': 'ללחוש',
    'he': 'ללחוש',
    'hu': 'Tülk',
    'hy': 'Թթել',
    'io': 'Siflar',
    'ja': 'トゥート',
    'ko': '툿',
    'no': 'Tut',
    'oc': 'Tut',
    'pl': 'Wyślij',
    'pt-BR': 'Publicar',
    'pt': 'Publicar',
    'ru': 'Трубить',
    'sr-Latn': 'Tutni',
    'sr': 'Тутни',
    'uk': 'Дмухнути',
    'zh-CN': '嘟嘟',
    'zh-HK': '發文',
    'zh-TW': '貼掉',
    'default': 'Toot'
  }

  let text = null
  try {
    text = publish[language]
  }
  catch (error) {
    text = publish.default
  }

  return text
}

